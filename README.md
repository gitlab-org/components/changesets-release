# `changesets-release` component

The component creates a `changesets-release` job which automatically releases new versions of a component on the default branch.

## Usage

To utilize this component, add it to an existing `.gitlab-ci.yml` file using the `include:` keyword:

```yaml
include:
  - component: gitlab.com/gitlab-org/components/changesets-release/changesets-release/@~latest
  # or a specific version
  - component: gitlab.com/gitlab-org/components/changesets-release/changesets-release@<VERSION>
```

## Inputs

| Input       | Default Value    | Description                                                  |
|-------------|------------------|--------------------------------------------------------------|
| `job_image` | `node:20-alpine` | The node version to use for the `changesets-release` job.    |
| `job_stage` | `publish`        | CI Pipeline stage where `changesets-release` job should run. |

### Example

* `.gitlab-ci.yml`

```yaml
include:
  - component: "gitlab.com/gitlab-org/components/changesets-release/changesets-release@~latest"
    inputs:
      node_major: "22"
```

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components
